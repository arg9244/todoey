import 'package:flutter/material.dart';
import 'package:todoey/models/task_data.dart';
import '../constants.dart';
import 'package:provider/provider.dart';

class AddTaskScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String newTaskName;

    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
      decoration: kTaskBoxDecoration,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            'Add Task',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 30.0,
            ),
          ),
          TextField(
            autofocus: true,
            textAlign: TextAlign.center,
            onChanged: (value) => newTaskName = value,
          ),
          SizedBox(height: 20.0),
          SizedBox(
            height: 50.0,
            child: FlatButton(
              child: Text(
                'Add',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                ),
              ),
              color: Theme.of(context).primaryColor,
              onPressed: () {
                if (newTaskName != null) {
                  Provider.of<TaskData>(context).addNewTask(newTaskName);
                }
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }
}
